# Utilisation de l'application

Il y a 2 modes de fonctionnement.
    - Autonome : l'application fonctionne seule sans backend. Les données ne sont pas persistantent
    - Avec Backend : l'application opère des requête REST API sur le backend http://localhost:8055/

## Available Scripts

Il y a 2 scripts suivant le mode d'utilisation de l'application

### `npm start`

Lance l'application en mode autonome

### `npm run prod`

Lance l'application qui utilise l'API REST

### Utiliation des fichier de lancement

* run.bat lance l'application qui utilise L'API REST

* run_serverless.bat lance l'application en mode autonome
