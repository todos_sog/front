export interface TodoModel {
  id?: number;
  title: string;
  description?: string;
  completed: number;
  updated_at?: Date;
}
