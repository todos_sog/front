import CommonEdition from "../../components/CommonEdition/CommonEdition";

const EditTodo = () => {
  return <CommonEdition title="Modifier la tâche" />;
};

export default EditTodo;
