import CommonEdition from "../../components/CommonEdition/CommonEdition";

const AddTodo = () => {
  return <CommonEdition title="Ajouter une tâche" />;
};

export default AddTodo;
