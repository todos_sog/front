const todos = [
  {
    id: 1,
    title: "Learn JavaScript",
    description: "No Typescript",
    completed: 1,
  },
  { id: 2, title: "Learn Vue", description: "See or not to see", completed: 0 },
  {
    id: 3,
    title: "Build something awesome",
    description: "You are Elon Musk ?",
    completed: 0,
  },
];

export default todos;
