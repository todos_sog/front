import "./Header.css";

const Header = () => {
  return (
    <header>
      <h1>ATMA</h1>
      <h2>Amazing Todos-list MAnagement</h2>
    </header>
  );
};

export default Header;
